# Developer documentation

Gitlab deposit is at https://gitlab.com/esrifrance-public/orionpy/orionpy

## Install the required modules as a developer

`pip install -r dev-requirements.txt`

## Generate the documentation

The documentation is written using docstring. It is generated using Sphinx

To regenerate the documentation, run the following from the **docs** folder, :

```
sphinx-quickstart 
sphinx-apidoc -f -o source/ ../mypackage/
make html
```
It will then be generated in the docs/build/html folder.

## Update the Demonstration using Jupyter Notebook

Go to OrionPy folder with a terminal, launch :
```
jupyter-notebook
```

## Run the tests
Some unitary tests have been written (in the **tests** folder) using the unittest module.

To launch the unitary tests, the list of dependencies is in the *dev-requirements.txt* file
Those dependencies are installed using :
```bash
pip install -r dev-requirements.txt
```
Then, the tests are run launching :
```bash
nosetests -v
```