"""
Module containing global variables
"""

# Boolean saying if the ArcGIS installation system is federated or not.
is_federated = True
