class BuildFilterError(ValueError):
    """Exception class to raise if an error occurs while creating a filter"""
    def foo(self):
        pass


class FilteringValuesError(ValueError):
    """Exception class to raise if an error occurs while setting filtering values"""
    def foo(self):
        pass
