# Pourquoi ces scripts

Les scripts Python disponibles dans ce répertoire permettent de maintenir à jour les informations associées aux unités d'organisation d'arcOpole Builder.
Ils sont fournis à titre d'exemple et peuvent être modifiés en fonction de vos besoin.

# Remerciements

L'équipe arcOpole tient à remercie la CA de Val Parisis qui a initié les premiers scripts et a bien voulu les transmettre pour relecture et enrichissement.

# Participer 

Vous aussi, vous pouvez allonger cette liste en fonction de vos besoins et en faire profiter la communauté arcOpole en prenant contact avec nous (arcopole@esrifrance.fr) ou sur GitLab : https://gitlab.com/esrifrance-public/orionpy/orionpy/-/tree/master/scripts