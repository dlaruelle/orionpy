from orionpy.orioncore.Orion import Orion
from orionpy.orioncsv.csvorganisationunit import CSVOrganisationUnit

# Exécuter avec : python demo_seminaire.py
if __name__ == '__main__':
    # Se connecte au serveur aOB en récupérant une instance de la classe Orion.
    orion = Orion("admin_aob", "", "https://demo.arcopole.fr")
    # Créé un FDU avec les valeurs de filtrage associées pour nos unités de l'organisation
    orion.filters.add_FDU_filter("Unités de l'organisation", fields=['organisation'],
                                 filtering_values = ["Transition énergétique", "Attractivité - Développement économique",
                                                     "Centre communal d'action sociale", "Culture - Patrimoine",
                                                     "Espaces Publics - Cadre de Vie", "Jeunesse - Vie sportive",
                                                     "Politiques territoriales"])

    # Récupère la ressource de suivi des connexions et le FDU créé
    stats_resource = orion.businesses.get_stats_resource("hosted")
    fdu_ou = orion.filters.get("Unités de l'organisation")
    # Associe le filtre à la ressource de suivi des connexions
    stats_resource.update_filter(fdu_ou.get_id())

    # Génère un CSV récapitulatif des utilisateurs et des unités de l'organisation associées.
    csv_handler = CSVOrganisationUnit(orion)
    csv_handler.generate("./demo_seminaire_sortie.csv")

    # Lit le csv et applique les modifications sur le serveur arcOpole.
    # csv_handler.read_and_apply("./demo_seminaire_entree.csv")
