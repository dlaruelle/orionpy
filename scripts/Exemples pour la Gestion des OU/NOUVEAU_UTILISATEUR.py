#Script python permettant d'ajouTer à une une liste FDU existante, une unité de l'organisation

from orionpy.orioncore.Orion import Orion
from orionpy.orioncsv.csvorganisationunit import CSVOrganisationUnit

# Exécuter avec : python demo_seminaire.py
if __name__ == '__main__':

    ##### Propriétés à changer avant d'executer le script ####

    # Login administrateur
    username = "login"
    # Mot de passe adminstrateur
    password = "mot de passe"
    # Url du serveur
    url_machine = "https://monserveur.fr"
    # Nom du webadaptor du portail
    portal = "portal"

    # Login de l'utilisateur à ajouter
    login = "login utilisateur"
    # Nom de l'unité d'organisation à associer
    ou = "Nom de l'unité d'org"

    ##########################################################

    # Se connecte au serveur aOB en récupérant une instance de la classe Orion.
    orion = Orion(username, password, url_machine, portal)

    # Recherche de l'utilisateur à mettre à jour
    user = orion.users.get_with_id(login)
    print(user)

    # Recherche de la ressource de stats
    stats_resource = orion.businesses.get_stats_resource("hosted")
    print(stats_resource)

    # Recherche du filtre associé à ressource de stats
    fdu_filter = orion.filters.get_with_id(stats_resource.filter_id)
    print(fdu_filter)

    # Mise à jour de la valeur pour l'utilisateur
    filter_values = [ou]
    user.set_filter_values(fdu_filter, filter_values)


	
	
	
	
