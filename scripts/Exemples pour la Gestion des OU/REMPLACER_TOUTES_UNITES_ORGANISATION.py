#Script python permettant d'ajouTer à une une liste FDU existante, une unité de l'organisation

from orionpy.orioncore.Orion import Orion
from orionpy.orioncsv.csvorganisationunit import CSVOrganisationUnit

# Exécuter avec : python demo_seminaire.py
if __name__ == '__main__':
    
    ##### Propriétés à changer avant d'executer le script ####

    # Login administrateur
    username = "login"
    # Mot de passe adminstrateur
    password = "mot de passe"
    # Url du serveur
    url_machine = "https://monserveur.fr"
    # Nom du webadaptor du portail
    portal = "portal"

    # Nom du filtre existant
    filter_name = "Unité organisationnelle"
    # Liste des nouvelles organisations
    new_values = ["Unité1", "Unité2", "Unité3"]

    ##########################################################

    # Se connecte au serveur aOB en récupérant une instance de la classe Orion.
    orion = Orion(username, password, url_machine, portal)
    
    # Ajout de la nouvelle valeur
    orion.filters.replace_fdu_values(filter_name, new_values)



	
	
	
	
