#Script python permettant de soumettre le CSV modifié au serveur arcOpole Builder pour prise en compte immédiate
from orionpy.orioncore.Orion import Orion
from orionpy.orioncsv.csvorganisationunit import CSVOrganisationUnit
	

##### Propriétés à changer avant d'executer le script ####

# Login administrateur
username = "login"
# Mot de passe adminstrateur
password = "mot de passe"
# Url du serveur
url_machine = "https://monserveur.fr"
# Nom du webadaptor du portail
portal = "portal"

# Fichier source
src = ".../utilisateurs.csv"

##########################################################

# Se connecte au serveur aOB en récupérant une instance de la classe Orion.
orion = Orion(username, password, url_machine, portal)

csv_rights = CSVOrganisationUnit(orion)
csv_path = src
csv_rights.read_and_apply(csv_path)


