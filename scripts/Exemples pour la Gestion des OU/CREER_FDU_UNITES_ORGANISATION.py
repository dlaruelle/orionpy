#Script python permettant de créér une liste FDU et des unités de l'organisation associées

#importer les modules
from orionpy.orioncore.Orion import Orion
from orionpy.orioncsv.csvorganisationunit import CSVOrganisationUnit

if __name__ == '__main__':

    ##### Propriétés à changer avant d'executer le script ####

    # Login administrateur
    username = "login"
    # Mot de passe adminstrateur
    password = "mot de passe"
    # Url du serveur
    url_machine = "https://monserveur.fr"
    # Nom du webadaptor du portail
    portal = "portal"

    # Nom du filtre
    filter_name = "Unité organisationnelle"
    # Valeurs de l'unité d'organisation
    filtering_values = ["Beauchamp","Bessancourt","Cormeilles en Parisis"]

    ##########################################################

    # Se connecte au serveur aOB en récupérant une instance de la classe Orion.
    orion = Orion(username, password, url_machine, portal)

    # Créé un FDU avec les valeurs de filtrage associées pour nos unités de l'organisation
    orion.filters.add_FDU_filter(filter_name, fields=['Organization'],
                                 filtering_values = filtering_values)

  

	
	
	
	
