#Script python permettant de récupérer la liste des utilisateurs et leur unité d’organisation associé dans un fichier CSV

from orionpy.orioncore.Orion import Orion
from orionpy.orioncsv.csvorganisationunit import CSVOrganisationUnit

# Exécuter avec : python demo_seminaire.py
if __name__ == '__main__':
    ##### Propriétés à changer avant d'executer le script ####

    # Login administrateur
    username = "login"
    # Mot de passe adminstrateur
    password = "mot de passe"
    # Url du serveur
    url_machine = "https://monserveur.fr"
    # Nom du webadaptor du portail
    portal = "portal"

    # Fichier source
    src = ".../utilisateurs.csv"
    # Nom du filtre
    filter_name = "Unité organisationnelle"

    ##########################################################

    # Se connecte au serveur aOB en récupérant une instance de la classe Orion.
    orion = Orion(username, password, url_machine, portal)

    # Récupère la ressource de suivi des connexions et le FDU créé
    stats_resource = orion.businesses.get_stats_resource("hosted")
    fdu_ou = orion.filters.get(filter_name) # à modifier si besoin
    # Associe le filtre à la ressource de suivi des connexions
    stats_resource.update_filter(fdu_ou.get_id())

    # Génère un CSV récapitulatif des utilisateurs et des unités de l'organisation associées.
    csv_handler = CSVOrganisationUnit(orion)
    csv_handler.generate(".../utilisateurs.csv")

	
	
	
	
