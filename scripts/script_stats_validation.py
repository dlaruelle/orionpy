# coding: utf-8

import sys
from sys import exit

from datetime import datetime, timedelta

from orionpy.orioncore.Orion import Orion



def main():
    # ------- Orion connection tests -------
    username = ""  # Enter here username
    password = ""  # If you want, enter password (if empty, will be asked - securely - later)
    url_machine = ""  # External url of the WebAdaptor used
    portal = "portal"  # Entry point to the WebAdaptor of the Portal for ArcGIS (optional)

    orion = Orion(username, password, url_machine, portal, verify_cert = False)

    now = datetime.now()
    yesterday = now - timedelta(1)
    last_year = now.replace(year = now.year - 1)

    stats_resource = orion.businesses.get_stats_resource("hosted")
    if not stats_resource or not stats_resource.service_url:
        return "No configuration for stats resource"

    # Check status 
    print("--------Check status-----------")
    status = stats_resource.get_status()
    search_operations = [
        "synthesis",
        "clean",
        "newSession",
        "heartBeat",
        "push",
        "query"
    ]
    operations = status.get("operations")
    if not search_operations == operations:
        return "All operations are not available"
    
    search_rules = [
        "CleanCacheLogs",
        "AnonymisesLogs",
        "MonthSynthesis",
        "CacheLogs"
    ]
    rules = status.get("rules")
    if not search_rules == rules:
        return "All rules are not available"

    # Push to clear cache
    stats_resource.push()

    print("--------Create log-----------")

    # Try to generate log for username
    session_id = stats_resource.create_new_session({
        "app": {
            "name": "TestIntegration"
        }
    })
    if not session_id:
        return "No session id create"

    stats_resource.heart_beat({
        "sessionId": session_id,
        "userName": username,
        "app": {
            "name": "TestIntegration"
        },
        "time": {
            "init": datetime.strftime(yesterday, '%Y-%m-%d %H:%M:%S') + " +01:00"
        }
    })

    push_result = stats_resource.push()
    add_results = push_result.get("addResults")
    if not len(add_results) == 1:
        return "Log not push correctly"

    # Synthesis
    synthesis_result = stats_resource.synthesis()
    if not synthesis_result.get("message") == "Success to synthetise logs":
        return "Synthesis process issue"

    print("--------Update log-----------")

    # Update log
    stats_resource.heart_beat({
        "sessionId": session_id,
        "userName": username,
        "app": {
            "name": "TestIntegration"
        },
        "time": {
            "init": datetime.strftime(last_year, '%Y-%m-%d %H:%M:%S') + " +01:00"
        }
    })

    push_result = stats_resource.push()
    update_results = push_result.get("updateResults")
    if not len(update_results) == 1:
        return "Log not push correctly"

    print("--------Clean generated log-----------")
    # Clean
    clean_result = stats_resource.clean()
    if clean_result.get("message") == "No logs to delete":
        return "Clean process issue"

    # Force synthesis to revert change
    stats_resource.synthesis()

    return 0


if __name__ == '__main__':
    sys.exit(main())
