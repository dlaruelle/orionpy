# Orionpy - Scripts

Sample scripts to use Orionpy

## Stats validation

This script validates the stats module installation. It checks Orion handler and generate a test stats data.

After use this script, need to delete data directly in FeatureService